*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}

*** Variables ***

*** Test Cases ***
TestAirFrance

    [Documentation]   Checks accessibility to https://wwws.airfrance.fr
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}

    Location Should Contain    ${Global.Url}
    Click Element     xpath://*[@id="mat-input-1"]
    Input Text     id:mat-input-1      Lyon
    # rajouter keyword pour cliquer sur le mene déroulant puisque rôle combobox
    Click Element     xpath://*[@id="mat-input-2"]
    Input Text     id:mat-input-2      Nice
    # rajouter keyword pour cliquer sur le mene déroulant puisque rôle combobox
    Click Element       xpath://*[@id="bw-search-widget-form-15hCmh4vxh"]/div[3]/div/button
    
    Close Browser

*** Keywords ***