*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI Heroku ${DSname}

*** Variables ***

# On peut ajouter ici des variables au besoin

*** Test Cases ***
TestHeroku

    #In SeleniumLibrary, browser is a synonym for WebDriver instance. Windows are browser children.
    [Documentation]   Checks accessibility to https://testpages.herokuapp.com/styled/validation/input-validation.html
    # ${Global.Url} va chercher sa valeur dans le fichier yml qui sert de dataset
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}

    Location Should Contain    ${Global.Url}
    Click Element      xpath://input[@id='firstname']
    Input Text     id:firstname    Admin 
    Click Element       xpath://input[@id='surname']
    Input Text    id:surname     admin123456789
    Click Element    xpath://input[@id='age']
    Input Text    id:age   29
    Click Element    xpath://input[@type='submit']
    Capture Page Screenshot
    
    Close Browser

*** Keywords ***

# On peut ajouter des mots-clés ici pour les utiliser dans un autre fichier .robot
