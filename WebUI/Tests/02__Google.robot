*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}

*** Variables ***

*** Test Cases ***
TestGoogle

    [Documentation]   Checks accessibility to https://www.google.fr
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    Location Should Contain    ${Global.Url}

    Click Element       xpath://button[@id='W0wltc']//div[@role='none']
    
    Click Element       xpath://textarea[@id='APjFqb']
    
    Input Text      id:APjFqb       Linux
    
    Click Element       xpath://div[@id='jZ2SBf']//div[1]
    
    Click Link      xpath://a[@href='https://www.linux.org/'][1]

    Capture Page Screenshot         filename=selenium-screenshot-{index}.png

    Close Browser

*** Keywords ***
