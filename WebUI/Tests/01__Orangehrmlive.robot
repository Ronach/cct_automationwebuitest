*** Settings ***
Library            SeleniumLibrary
Resource           CCT_Selenium.resource
Resource           CCT_Keywords.resource
Force Tags         WebUI ${DSname}

*** Test Cases ***
TestOrange

    #In SeleniumLibrary, browser is a synonym for WebDriver instance.
    #Windows are children of a browser
    [Documentation]   Checks accessibility to https://opensource-demo.orangehrmlive.com/
    CCT Open Browser    ${Global.Url}   ${Global.Browser}    ${Global.Version}
    

    Location Should Contain    ${Global.Url}
    Click Element      xpath://input[@class="oxd-input oxd-input--active"]
    Input Text     class:username    Admin
    Click Element       xpath://input[@class="oxd-input oxd-input--active"]
    Input Element    class:password      admin123
    Click Element    xpath://button[@type='submit']
    Capture Page Screenshot
    

    Close Browser

*** Keywords ***
